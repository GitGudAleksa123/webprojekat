Vue.component("error-403", {
  data() {
    return {};
  },
  template: `
  <div>
  <p style="font-size: 40px"><b>ERROR 403 FORBIDEN ACCESS!</b></p>
  <a href="#/login">Go to login page</a>
  </div>
  `,
});
