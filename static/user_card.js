Vue.component("user-card", {
  props: ["user"],
  template: `
    <div class="card content-card">
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <img :src="user.profilePicture.path" class="rounded profile-image">
                    </div>
                    <div class="col">
                        <h5>{{ user.username }}</h5>
                        <p class="card-text"> {{ user.name }} {{ user.surname }}</p>
                        <p class="card-text"> {{ user.email }}</p>
                        <p class="card-text"> {{ user.dateOfBirth }}</p>
                        <p class="card-text"> {{ user.gender }}</p>
                    </div>
                    
                </div>
            </div>
            <a :href="'/#/regular/profile/' + user.username" class="btn btn-outline-dark">Profile</a>
            
        </div>
    </div>
      `,
});
