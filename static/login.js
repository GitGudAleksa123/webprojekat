Vue.component("login-page", {
  data: function () {
    return {
      username: "",
      password: "",
      error: false,
    };
  },
  template: `
  <div>
    <home-navbar></home-navbar>
    <div class="d-flex justify-content-center">

      <div class="card content-card login-card">
        <div class="card-header">
          <h5><b>Login</b></h5>
        </div>
        <div class="card-body">
          <div class="container">
            <div class="col">
                <div class="mb-3">
                  <label for="usernameField" class="form-label">Username</label>
                  <input type="text" class="form-control" id="usernameField" v-model="username">
                </div>
                <div class="mb-3">
                  <label for="passwordField" class="form-label">Password</label>
                  <input type="password" class="form-control" id="passwordField" aria-describedby="loginCredentials" v-model="password">
                  <div id="loginCredentials" class="form-text"><span style="color:red" v-if="error">Invalid username or password.</span></div>
                </div>
                <div>
                  <button class="btn btn-outline-dark" v-on:click="login(username, password)">Login</button>  
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    `,
  methods: {
    login: function (username, password) {
      axios
        .post("api/login", { username: username, password: password })
        .then((res) => {
          if (res.status == 200) window.location.href = "#/regular";
          else {
            alert(res.data);
            this.error = true;
          }
        })
        .catch((err) => {
          alert(err);
          this.error = true;
        });
    },
  },
});
