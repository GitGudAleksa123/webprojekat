Vue.component("chat", {
  data() {
    return {
      socket: "",
      user: [],
      recipient: "",
      text: "",
      messages: [],
      profilePhotosMap: new Map(),
    };
  },
  template: `
    <div>
      <regular-navbar></regular-navbar>
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col">

            <div class="card chat">
              <div class="container message-container">
                <div class="row" v-for="message in messages">
                
                  <div class="col" v-if="message.sender == user.username">
                    <p class="date-text right-info"> {{ message.sender }} at {{ message.dateOfMessage }}</p>
                    <div class="card text-card right-card right-text">
                      <p class="message-text">{{ message.text }}</p>
                    </div>
                  </div>

                  <div class="col" v-else>
                    <p class="date-text left-info"> {{ message.sender }} at {{ message.dateOfMessage }}</p>
                    <div class="card text-card left-card">
                      <p class="message-text">{{ message.text }}</p>
                    </div>
                  </div>

                </div>
              </div>

              <div class="container">
                <div class="row text-card">
                  <input type="text" class="form-control" placeholder="Enter message" v-model="text" v-on:keyup.enter="sendMessage()">
                </div>
              </div>
            </div>

          </div>
          
          <div class="col">
            <div class="card friend-list-card">
              <div class="card-header">
                <h5 class="friend-header">Friends</h5>
              </div>
              <div class="card-body">
                <ul class="list-group" v-for="friend in user.friends">
                  <li class="list-group-item">
	                    <img :src="profilePhotosMap[friend]" @click="openProfile(friend)"
	                        :style="{cursor: 'pointer', width: '50px', height: '50px', objectFit: 'contain', borderRadius: '50%', marginLeft: '3px'}" />
	                        <a class="friend-header" v-on:click="openChat(friend)"><b v-if="recipient == friend">{{ friend }}</b><span v-else>{{ friend }}</span></a>
	              </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  `,
  beforeMount: function () {
    axios.get("api/get-profile-photos").then((res) => {
      this.profilePhotosMap = res.data;
    });
    axios
      .get("api/current-user")
      .then((res) => {
        this.user = res.data;
        this.recipient = this.user.friends[0];
        this.openChat(this.recipient);
      })
      .catch((err) => {
        console.error(err);
      });
    this.readMessage();
  },
  methods: {
    updateChat: function (message) {
      var messageObj = JSON.parse(message.data);
      this.messages.push(messageObj);
      window.setTimeout(() => {
        this.getMessages();
      }, 400);
    },
    sendMessage: function () {
      var messageObj = {
        text: this.text,
        sender: this.user.username,
        recipient: this.recipient,
        isDeleted: false,
      };
      this.socket.send(JSON.stringify(messageObj));
      this.text = "";
      window.setTimeout(() => {
        this.getMessages();
      }, 400);
    },
    openChat: function (friend) {
      this.recipient = friend;
      this.getMessages();
      this.createWebSocket();
    },
    createWebSocket: function () {
      this.socket = new WebSocket(
        "ws://" + location.hostname + ":" + location.port + "/chat"
      );
      this.socket.onopen;
      this.socket.onmessage = this.updateChat;
      this.socket.onclose = function () {
        alert("WEB SOCKET CLOSED");
      };
    },
    getMessages: function () {
      axios
        .post("api/messages", this.recipient)
        .then((res) => {
          this.messages = res.data;
        })
        .catch((err) => {
          console.error(err);
        });
    },
    readMessage: function () {
      axios
        .get("api/read-message")
        .then((res) => {})
        .catch((err) => {
          console.error(err);
        });
    },
    openProfile: function (username) {
      this.$router.push("/regular/profile/" + username);
      window.location.reload();
    },
  },
});
