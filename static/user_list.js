Vue.component("user-list", {
  data() {
    return {
      field: "",
      from: "1900-01-01",
      to: "2022-01-01",
      users: [],
    };
  },
  template: `
    <div class="container">
        <div class="d-flex justify-content-center">
          <div class="card content-card search-card">
            <div class="container">
              <div class="row">
                <div class="col"><input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" v-model="field"></div>
                <div class="col"><input class="form-control" type="date" placeholder="From" v-model="from"></div>
                <div class="col"><input class="form-control" type="date" placeholder="From" v-model="to"></div>
                <div class="col"><button class="btn btn-outline-dark" v-on:click="search()">Search</button></div> 
              </div>
              <div class="row">
                <div class="dropdown">
                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    Sort by
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" v-on:click="sortByName()">Name</a></li>
                    <li><a class="dropdown-item" v-on:click="sortBySurname()">Surname</a></li>
                    <li><a class="dropdown-item" v-on:click="sortByDate()">Date of birth</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="d-flex justify-content-center" v-for="user in users" :key="user.username">
            <user-card :user="user"></user-card>
        </div>
    </div>
    `,
  beforeMount: function () {
    axios
      .get("api/get-public-users")
      .then((res) => {
        if (res.status == 200) {
          this.users = res.data;
        }
      })
      .catch((err) => {
        console.error(err);
        window.location.replace("http://localhost:3000/#/error-403");
      });
  },
  methods: {
    search() {
      axios
        .post("api/search", {
          field: this.field,
          from: this.from,
          to: this.to,
        })
        .then((res) => {
          this.users = res.data;
        })
        .catch((err) => {
          console.error(err);
        });
    },
    sortByName: function () {
      this.users.sort((a, b) => a.name.localeCompare(b.name));
    },
    sortBySurname: function () {
      this.users.sort((a, b) => a.surname.localeCompare(b.surname));
    },
    sortByDate: function () {
      this.users.sort(
        (a, b) => new Date(a.dateOfBirth) - new Date(b.dateOfBirth)
      );
    },
  },
});
