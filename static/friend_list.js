Vue.component("friend-list", {
  props: ["friends"],
  template: `
        <div>
            <div class="container">
              <div class="col">
              </div>
              <div class="col">
              </div>
              <div class="col">
              </div>
            </div>
        </div>
      `,
  beforeMount: function () {
    axios
      .get("api/get-current-user-chats")
      .then((res) => {
        if (res.status == 200) {
          this.user = res.data;
        }
      })
      .catch((err) => {
        console.error(err);
        window.location.replace("http://localhost:3000/#/error-403");
      });
  },
});
