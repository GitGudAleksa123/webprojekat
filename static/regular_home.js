Vue.component("regular-home", {
  data() {
    return {
      username: "",
	  newPostType: '',
	  newPostPlaceholder: '',
	  createNewPost: false,
	  profilePhotosMap: new Map(),
	  postsList: [],
	  loggedProfilePicture: "",
    };
  },
  template: `
    <div>
        <regular-navbar :username="username"></regular-navbar>
		<div class="container">
	        <div class="row" :style="{padding: '10px'}">
	            <div class="col-sm">
	                <button @click="newPost('POST')"
	                    :style="{width: '100%', height: '100px', marginLeft: 'auto', marginRight: 'auto', fontSize: '2.5rem'}"
	                    class="btn btn-primary post-button"><svg xmlns="http://www.w3.org/2000/svg"
	                        :style="{marginRight: '20px', marginBottom: '8px'}" width="40" height="40" fill="currentColor"
	                        class="bi bi-card-text" viewBox="0 0 16 16">
	                        <path
	                            d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
	                        <path
	                            d="M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8zm0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z" />
	                    </svg>New Post</button>
	            </div>
	            <div class="col-sm">
	                <button @click="newPost('IMAGE')"
	                    :style="{width: '100%', height: '100px', marginLeft: 'auto', marginRight: 'auto', fontSize: '2.5rem'}"
	                    class="btn btn-primary post-button"><svg xmlns="http://www.w3.org/2000/svg"
	                        :style="{marginRight: '20px', marginBottom: '8px'}" width="40" height="40" fill="currentColor"
	                        class="bi bi-card-image" viewBox="0 0 16 16">
	                        <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
	                        <path
	                            d="M1.5 2A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13zm13 1a.5.5 0 0 1 .5.5v6l-3.775-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12v.54A.505.505 0 0 1 1 12.5v-9a.5.5 0 0 1 .5-.5h13z" />
	                    </svg>New Image</button>
	            </div>
	        </div>
	        <div class="container" v-if="createNewPost">
	            <div class="mx-auto"
	                :style="{textAlign: 'center', border: '1px solid gray', borderRadius: '20px', backgroundColor: 'rgb(13, 110, 253)', color: 'white', fontSize: '22px',height: '200px', width: '85%'}">
	                <label class="ms-auto" for="post-image">Upload an image {{newPostType==='POST' ? '(optional)' :
	                    ''}}</label>
	                <input type="file" class="form-control.file" id="post-image" name="post-image">
	                <textarea id="textInput" name="textInput"
	                    :style="{width: '90%', margin: 'auto', height: '50%', resize: 'none', fontSize: '22px'}"
	                    class="form-control" :placeholder="newPostPlaceholder"></textarea>
	                <div :style="{marginTop: '10px', transition: '0.4s'}">
	                    <button class="btn btn-secondary btn-lg" @click="postNewPost">Post</button>
	                    <button class="btn btn-secondary btn-lg" @click="createNewPost=false">Cancel</button>
	                </div>
	            </div>
	        </div>
	    </div>
	
	    <ul class="container" style="list-style: none;">
	        <li v-for="post in postsList" style="margin: 10px 0px;">
	            <div class="card mx-auto" v-if="!post.isDeleted">
	                <span class="d-flex">
	                    <img :src="profilePhotosMap[post.author]" @click="openProfile(post.author)"
	                        :style="{cursor: 'pointer', width: '50px', height: '50px', objectFit: 'contain', borderRadius: '50%', marginLeft: '3px'}" />
	                    <h6 class="my-auto ms-3">{{post.author}}</h6>
	                    <p class="my-auto ms-3">{{post.timeCreated}}</p>
	                    <button v-if="post.author===logged" :style="{marginLeft: '8px', border: '0px', background: 'white'}"
	                        @click="editPost(post.id)"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
	                            fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
	                            <path
	                                d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z" />
	                        </svg> </button>
	                    <button v-if="userType==='ADMIN' || post.author===logged"
	                        :style="{marginLeft: '8px', border: '0px', background: 'white'}" @click="deletePost(post.id)">
	                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
	                            class="bi bi-trash" viewBox="0 0 16 16">
	                            <path
	                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
	                            <path fill-rule="evenodd"
	                                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
	                        </svg> </button>
	                    <p v-if="post.edited">Edited</p>
	                    <button :id="'button' + post.id" class="my-auto" :style="{height: '80%', marginLeft: '20px'}"
	                        @click="showDetails(post.id, post.postType)">Show Details</button>
	                </span>
	
	                <img :id="'img' + post.id" v-if="typeof post.photo.path !== 'undefined'" class="card-img-top mx-auto"
	                    :style="post.postType==='POST' ? 'width: 80%; height: 80%; display: none;' : 'width: 80%; height: 80%; display: block;'"
	                    :src="post.photo.path" />
	                <div :style="post.postType==='POST' ? 'display: block;' : 'display: none;'" :id="'text' + post.id"
	                    class="card-body">
	                    <h5 :id="'post-input' + post.id">{{post.description}}</h5>
	                </div>
	
	                <ul :id="'comments' + post.id" class="list-group list-group-flush"
	                    style="list-style: none; display: none;">
	                    <li v-for="comment in post.comments" :style="{paddingLeft: '10px', borderBottom: '1px solid gray'}">
	                        <span v-if="!comment.isDeleted">
	                            <div class="d-flex">
	                                <img :src="profilePhotosMap[comment.author]" @click="openProfile(comment.author)"
	                                    :style="{cursor: 'pointer', width: '50px', height: '50px', objectFit: 'contain', borderRadius: '50%'}" />
	                                <h6 class="card-title my-auto ms-3">{{comment.author}}</h6>
	                                <p class="my-auto ms-3">{{comment.edited ? comment.dateOfEdit : comment.dateOfComment}}
	                                </p>
	                                <button v-if="comment.author===logged"
	                                    :style="{marginLeft: '8px', border: '0px', background: 'white'}"
	                                    @click="editComment(comment.id)"> <svg xmlns="http://www.w3.org/2000/svg" width="16"
	                                        height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
	                                        <path
	                                            d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z" />
	                                    </svg> </button>
	                                <button v-if="userType==='ADMIN' || comment.author===logged"
	                                    :style="{marginLeft: '8px', border: '0px', background: 'white'}"
	                                    @click="deleteComment(comment.id)"> <svg xmlns="http://www.w3.org/2000/svg"
	                                        width="16" height="16" fill="currentColor" class="bi bi-trash"
	                                        viewBox="0 0 16 16">
	                                        <path
	                                            d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
	                                        <path fill-rule="evenodd"
	                                            d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
	                                    </svg> </button>
	                                <p v-if="comment.edited" :style="{marginLeft: '8px', marginTop: '8px'}">Edited</p>
	                            </div>
	                            <p :id="'comment' + comment.id" clas="ms-10">{{comment.text}}</p>
	                        </span>
	                    </li>
	
	                    <li class="list-group-item" v-if="logged !== 'GUEST'">
	                        <span class="d-flex">
	                            <img :src="loggedProfilePicture"
	                                :style="{width: '50px', height: '50px', objectFit: 'contain', borderRadius: '50%'}" />
	                            <input :id="'comment-input' + post.id" type="text" placeholder="Write a comment..."
	                                :style="{width: '100%', paddingLeft: '15px', lineHeight: '1.5', border: '1px solid #ced4da', borderRadius: '0.25rem'}">
	                            <button @click="postComment(post.id)" class="btn btn-secondary post-comment-new"
	                                :style="{marginLeft: '5px'}">Post</button>
	                        </span>
	                    </li>
	                </ul>
	            </div>
	
	        </li>
	    </ul>
    </div>
  `,
  methods: {
	newPost: function(postType) {
			this.createNewPost = true;
			this.newPostType = postType;
			this.newPostPlaceholder = postType === 'POST' ? 'Enter text' : 'Enter text (optional)';
		},
		postNewPost: function(e) {
			const text = document.querySelector("#textInput").value;
			const img = document.querySelector("#post-image");
			const validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
			let proceed = false;
			try {
				if (this.newPostType === "IMAGE") {
					if ((validImageTypes.includes(img.files[0]["type"]) || img.files.length != 0)) {
						proceed = true;
					} else {
						alert("Image must contain image!");
					}
				} else {
					if (text != "") {
						proceed = true;
					} else {
						alert("Post must contain some text!");
					}
				}
			} catch (error) {
				if (this.newPostType === "IMAGE") {
					if (img.files.length == 0) alert("You must choose image");
				} else {
					if (text == "") alert("You must enter text!");
				}
			}
			if (proceed) {
				let formData = new FormData();
				if (img.files.length != 0) {
					let file = img.files[0];
					formData.append("file", file);
				}
				axios
					.post("api/createPost", formData, {
						headers: {
							"Content-Type": "multipart/form-data"
						},
						params: { text: text, type: this.newPostType }
					})
					.then((res) => {
						this.postsList.unshift(res.data);
						this.createNewPost = false;
					})
					.catch((err) => {
						console.error(err);
						this.error = true;
					});
			}
		},
		editPost: function(postID) {
			let post = document.getElementById("post-input" + postID);
			let postText = post.innerHTML;
			let editedText = window.prompt("Edit post", postText);
			if (editedText == null || editedText == "" || editedText === postText) {
				return;
			} else {
				for (let p of this.postsList) {
					if (p.id == postID) {
						axios
						.post("api/updatePost",{ text: editedText, postID: p.id, owner: p.author})
						.then((res) => {
							p.description = editedText;
						})
						.catch((err) => {
							alert("Something went wrong!");
						})
						p.description = editedText;
						return;
					}
				}
			}
		},
		deletePost: function(postID) {
			for (let p of this.postsList) {
				if (p.id === postID) {
					axios
					.post("api/deletePost",{ postID: postID, owner: p.author})
					.then((res) => {
						p.isDeleted = true;
					})
					.catch((err) => {
						alert("Something went wrong!");
					})
					return;
				}
			}
		},
		postComment: function(postID) {
			let comment = document.getElementById("comment-input" + postID);
			let commentText = comment.value;
			if (commentText != "") {
				for (let p of this.postsList) {
					if (p.id === postID) {
						axios
							.post("api/postComment", { text: commentText, postID: postID, owner: p.author })
							.then((res) => {
								p.comments.push(res.data);
							})
							.catch((err) => {
								alert("Something went wrong!");
							})
						document.getElementById("comment-input" + postID).value = ""
						return;
					}
				}
			}
		},
		editComment: function(commentID) {
			let comment = document.getElementById("comment" + commentID);
			let commentText = comment.innerHTML;
			let editedText = window.prompt("Edit comment", commentText);
			if (editedText == null || editedText == "" || editedText === commentText) {
				return;
			} else {
				for (let p of this.postsList) {
					for (let c of p.comments) {
						if (c.id == commentID) {
							axios
							.post("api/updateComment",{ text: editedText, postID: p.id, commentID: c.id, owner: p.author})
							.then((res) => {
								c.text = editedText;
								c.edited = true;
								c.dateOfEdit = res.data;
							})
							.catch((err) => {
								alert("Something went wrong!");
							})
							c.text = editedText;
							return;
						}
					}
				}
			}
		},
		deleteComment: function(commentID) {
			for (let p of this.postsList) {
				for (let c of p.comments) {
					if (c.id == commentID) {
						axios
						.post("api/deleteComment",{ postID: p.id, commentID: c.id, owner: p.author})
						.then((res) => {
							c.isDeleted = true;
						})
						.catch((err) => {
							alert("Something went wrong!");
						})
						return;
					}
				}
			}
		},
		openProfile: function(username) {
			this.$router.push("/regular/profile/" + username);
			window.location.reload();
		},
		showDetails: function(id, postType) {
			let text = document.getElementById('text' + id);
			let img = document.getElementById('img' + id);
			let button = document.getElementById('button' + id);
			let comments = document.getElementById("comments" + id);
			if (postType === "POST") {
				if (img != null) {
					if (img.style.display === "none") {
						img.style.display = "block";
						button.innerHTML = "Hide Details";
					} else {
						img.style.display = "none";
						button.innerHTML = "Show Details";
					}
				}
			} else {
				if (text != null) {
					if (text.style.display === "none") {
						text.style.display = "block";
						button.innerHTML = "Hide Details";
					} else {
						text.style.display = "none";
						button.innerHTML = "Show Details";
					}
				}
			}
			if (comments != null) {
				if (comments.style.display === "none") {
					comments.style.display = "block";
					button.innerHTML = "Hide Details";
				} else {
					comments.style.display = "none";
					button.innerHTML = "Show Details";
				}
			}
		},
  },
  beforeMount: function() {
		axios
			.get("api/current-user")
			.then((res) => {
				if (res.status == 200) {
					this.logged = res.data.username;
					this.userType = res.data.type;
					this.loggedProfilePicture = res.data.profilePicture ? res.data.profilePicture.path : "static_resources/default_profile_pic.jpg";
					this.username = res.data.username;
				}
			})
		axios
			.get("api/get-friend-posts")
			.then((res) => {
				if (res.status == 200) {
					this.postsList = res.data;
					this.postsList.sort((a, b) => (a.timeCreated > b.timeCreated) ? -1 : 1)
				}
			})
		axios
			.get("api/get-profile-photos")
			.then((res) => {
				this.profilePhotosMap = res.data;
			})
	}
  
});
