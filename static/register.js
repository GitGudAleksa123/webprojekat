Vue.component("register-page", {
  data() {
    return {
      username: "",
      name: "",
      surname: "",
      email: "",
      password: "",
      repeatPassword: "",
      dateOfBirth: "",
      gender: "",
      registrationSent: false,
      userAlreadyExists: false,
    };
  },
  template: `
    <div>
    	<home-navbar></home-navbar>
      <div class="d-flex justify-content-center">
        <div class="card content-card register-card">
          <div class="card-header">
            <h5><b>Register</b></h5>
          </div>
          <div class="card-body">
            <div class="container">
              <div class="mb-3">
                <label for="usernameField" class="form-label">Username</label>
                <input type="text" class="form-control" id="usernameField" v-model="username">
                <div id="username-text" class="form-text"><span style="color:red" v-if="registrationSent && !isValidUsername()">Username can contain 4-20 characters and can have a (-) and (_).</span></div>
              </div>

              <div class="mb-3">
                <label for="nameField" class="form-label">Name</label>
                <input type="text" class="form-control" id="nameField" v-model="name">
                <div id="name-text" class="form-text"><span style="color:red" v-if="registrationSent && !isValidName()">Name must contain only characters and must contain 3-20 characters.</span></div>
              </div>

              <div class="mb-3">
                <label for="surnameField" class="form-label">Surname</label>
                <input type="text" class="form-control" id="surnameField" v-model="surname">
                <div id="surname-text" class="form-text"><span style="color:red" v-if="registrationSent && !isValidSurname()">Name must contain only characters and must contain 3-20 characters.</span></div>
              </div>

              <div class="mb-3">
                <label for="emailField" class="form-label">Email</label>
                <input type="email" class="form-control" id="emailField" v-model="email">
                <div id="email-text" class="form-text"><span style="color:red" v-if="registrationSent && !isValidEmail()">Invalid email.</span></div>
              </div>
              <div class="mb-3">
                <label for="passwordField" class="form-label">Password</label>
                <input type="password" class="form-control" id="passwordField" v-model="password">
                <div id="password-text" class="form-text"><span style="color:red" v-if="registrationSent && !doPasswordsMatch()">Passwords don't match.</span></div>
              </div>
              <div class="mb-3">
                <label for="rpasswordField" class="form-label">Repeat password</label>
                <input type="password" class="form-control" id="rpasswordField" v-model="repeatPassword">
                <div id="repeat-text" class="form-text"><span style="color:red" v-if="registrationSent && !doPasswordsMatch()">Passwords don't match.</span></div>
              </div>
              <div class="mb-3">
                <label for="dateOfBirthField" class="form-label">Date of birth</label>
                <input type="date" class="form-control" id="dateOfBirthField" v-model="dateOfBirth">
                <div id="date-text" class="form-text"><span style="color:red" v-if="!dateOfBirth">Invalid date of birth.</span></div>
              </div>
              <div class="mb-3">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                  <input type="radio" class="btn-check" id="male" value="MALE" checked v-model="gender">
                  <label class="btn btn-outline-dark" for="male">Male</label>

                  <input type="radio" class="btn-check" id="female" value="FEMALE" v-model="gender">
                  <label class="btn btn-outline-dark" for="female">Female</label>
                </div>
              </div>
              <button class="btn btn-outline-dark" v-on:click="register()">Register</button>
              <div id="username-text" class="form-text"><span style="color:red" v-if="userAlreadyExists">Username is already taken.</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    `,
  methods: {
    register: function () {
      if (
        this.doPasswordsMatch() &&
        this.isValidUsername() &&
        this.isValidEmail() &&
        this.isValidName() &&
        this.isValidSurname()
      )
        axios
          .post("api/register", {
            username: this.username,
            name: this.name,
            surname: this.surname,
            email: this.email,
            password: this.password,
            repeatPassword: this.repeatPassword,
            dateOfBirth: this.dateOfBirth,
            gender: this.gender,
          })
          .then((res) => {
            if (res.status === 200) {
              alert(res.data);
              window.location.href = "#/regular";
            } else {
              this.userAlreadyExists = true;
              alert(res.data);
            }
          })
          .catch((err) => {
            console.error(err);
          });
      this.registrationSent = true;
    },
    isValidEmail: function () {
      return String(this.email)
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
    },
    doPasswordsMatch: function () {
      return this.password === this.repeatPassword;
    },
    isValidUsername: function () {
      return String(this.username).match(/^[a-zA-Z0-9_-]{4,20}$/);
    },
    isValidName: function () {
      return String(this.name).match(/(^[a-zA-Z][a-zA-Z\s]{2,20}[a-zA-Z]$)/);
    },
    isValidSurname: function () {
      return String(this.surname).match(/(^[a-zA-Z][a-zA-Z\s]{2,20}[a-zA-Z]$)/);
    },
  },
});
