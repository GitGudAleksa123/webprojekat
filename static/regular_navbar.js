Vue.component("regular-navbar", {
  data() {
    return {
      user: {},
    };
  },
  props: ["username"],
  template: `
        <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#/regular">Projekat web</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav>
                  <li class="nav-item dropdown">
                    <a id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      <img src="static_resources/menu_FILL0_wght400_GRAD-25_opsz48.png" style="width: 40px;">
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <li><a class="dropdown-item" href="#/regular">Homepage</a></li>
                      <li><a class="dropdown-item" :href="'#/regular/profile/' + username">Profile</a></li>
                      <li><a class="dropdown-item" href="#/regular/chat">Chat <span style="color:red" v-if="user.hasUnreadMessage"> New message</span></a></li>
                      <li><a class="dropdown-item" href="#/regular/search">Search</a></li>
                      
                      <li><a class="dropdown-item" v-on:click="logout()">Logout</a></li>
                    </ul>
                  </li>
              </ul>
            </div>
        </div>
        </nav>
        </div>
    `,
  beforeMount: function () {
    this.getCurrentUser();
  },
  methods: {
    logout: function () {
      axios
        .post("api/logout", {})
        .then((res) => {
          if (res.status == 200) {
            window.location.href = "#/login";
            alert(res.data);
          } else if (res.status == 500) {
            window.location.replace("localhost:3000/#/error-500");
          }
        })
        .catch((err) => {
          console.error(err);
        });
    },
    getCurrentUser: function () {
      axios
        .get("api/current-user")
        .then((res) => {
          this.user = res.data;
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
});
