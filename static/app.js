const RegularNavbar = { template: "<regular-navbar></regular-navbar>" };
const HomeNavbar = { template: "<home-navbar></home-navbar>" };
const HomePage = { template: "<home-page></home-page>" };
const LoginPage = { template: "<login-page></login-page>" };
const RegisterPage = { template: "<register-page></register-page>" };
const RegularHomePage = { template: "<regular-home></regular-home>" };
const ErrorPage = { template: "<error-403></error-403>" };
const ProfilePage = { template: "<profile-page></profile-page>" };
const EditProfilePage = { template: "<edit-profile></edit-profile>" };
const UserList = { template: "<user-list></user-list>" };
const UserCard = { template: "<user-card></user-card>" };

const ChatPage = { template: "<chat></chat>" };
const Search = { template: "<search></search>" };

const router = new VueRouter({
  mode: "hash",
  routes: [
    { path: "/", component: HomePage },
    { path: "/login", component: LoginPage },
    { path: "/register", component: RegisterPage },
    { path: "/regular", component: RegularHomePage },
    { path: "/regular/profile/:id", component: ProfilePage },
    { path: "/error-403", component: ErrorPage },
    { path: "/regular/edit-profile", component: EditProfilePage },
    { path: "/regular/chat", component: ChatPage },
    { path: "/regular/search", component: Search },
  ],
});

var app = new Vue({
  router,
  el: "#index",
});
