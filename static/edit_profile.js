Vue.component("edit-profile", {
  data() {
    return {
      photos: [],
      profilePicture: "",
      username: "",
      oldPassword: "",
      newPassword: "",
      repeatPassword: "",
      name: "",
      surname: "",
      email: "",
      dateOfBirth: "",
      gender: "",
      isPrivate: "",
      editSent: false,
      images: [],
    };
  },
  template: `
  <div>
    <regular-navbar></regular-navbar>
    <div class="d-flex justify-content-center">
      <div class="card profile-card">
        <div class="container">

          <div class="row">
            <div class="col">
              <img :src="profilePicture" style="width: 250px; height: 250px">
            </div>
            <div class="col">
	            <div >
	              <div :style="{height: '230px', overflowY: 'scroll'}">
	                <table>
	                  <thead>
	                    <tr>
	                      <th> Your pictures </th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                    <tr v-for="img in images" v-on:click="selectImage(img)">
	                      <td><img :src="img" alt="Image" width="100" height="100" :style="{cursor: 'pointer'}"></td>
	                    </tr>
	                  </tbody>
	                </table>
	              </div>
	            </div>
	          </div>
          </div>

          <div class="row">
            <div class="col">
              <h5><b>{{ username }}</b></h5>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="mb-3">
                <label for="nameField" class="form-label">Name</label>
                <input type="text" class="form-control" id="nameField" v-model="name">
                <div id="name-text" class="form-text"><span style="color:red" v-if="editSent && !isValidName()">Name must contain only characters and must contain 3-20 characters.</span></div>
              </div>
            </div>

            <div class="col">
              <div class="mb-3">
                <label for="surnameField" class="form-label">Surname</label>
                <input type="text" class="form-control" id="surnameField" v-model="surname">
                <div id="surname-text" class="form-text"><span style="color:red" v-if="editSent && !isValidSurname()">Name must contain only characters and must contain 3-20 characters.</span></div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col">
              <div class="mb-3">
                <label for="emailField" class="form-label">Email</label>
                <input type="email" class="form-control" id="emailField" v-model="email">
                <div id="email-text" class="form-text"><span style="color:red" v-if="editSent && !isValidEmail()">Invalid email.</span></div>
              </div>
            </div>
            <div class="col">
              <div class="mb-3">
                <label for="dateOfBirthField" class="form-label">Date of birth</label>
                <input type="date" class="form-control" id="dateOfBirthField" v-model="dateOfBirth">
                <div id="date-text" class="form-text"><span style="color:red" v-if="!dateOfBirth">Invalid date of birth.</span></div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col">
              <div class="mb-3">
                <label for="passwordField" class="form-label">Old password</label>
                <input type="password" class="form-control" id="passwordField" v-model="oldPassword">
              </div>
            </div>
            <div class="col">
              <div class="mb-3">
                <label for="passwordField" class="form-label">New password</label>
                <input type="password" class="form-control" id="passwordField" v-model="newPassword">
                <div id="password-text" class="form-text"><span style="color:red" v-if="editSent && !doPasswordsMatch()">Passwords don't match.</span></div>
              </div>
            </div>
            <div class="col">
              <div class="mb-3">
                <label for="rpasswordField" class="form-label">Repeat new password</label>
                <input type="password" class="form-control" id="rpasswordField" v-model="repeatPassword">
                <div id="repeat-text" class="form-text"><span style="color:red" v-if="editSent && !doPasswordsMatch()">Passwords don't match.</span></div>
              </div>
            </div>
            <div class="col align-self-end" style="padding-bottom: 16px">
              <button class="btn btn-outline-dark" v-on:click="changePassword()">Change password</button>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <button class="btn btn-outline-dark" v-on:click="saveChanges()">Save Changes</button>
            </div>

            <div class="col">
              <div class="mb-3">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                  <input type="radio" class="btn-check" id="male" value="MALE" checked v-model="gender">
                  <label class="btn btn-outline-dark" for="male">Male</label>

                  <input type="radio" class="btn-check" id="female" value="FEMALE" v-model="gender">
                  <label class="btn btn-outline-dark" for="female">Female</label>
                </div>
              </div>
            </div>

            <div class="col">
              <div class="mb-3">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                  <input type="radio" class="btn-check" id="public" value="false" checked v-model="isPrivate">
                  <label class="btn btn-outline-dark" for="public">Public</label>

                  <input type="radio" class="btn-check" id="private" value="true" v-model="isPrivate">
                  <label class="btn btn-outline-dark" for="private">Private</label>
                </div>
              </div>
            </div>
            
            <div class="col">
              <button class="btn btn-outline-dark" v-on:click="deleteProfile()">Delete profile</button>
            </div>
            
          </div>

        </div>
      </div>
    </div>
  </div>
      `,
  beforeMount: function () {
    axios
      .get("api/current-user")
      .then((res) => {
        if (res.status == 200) {
          this.username = res.data.username;
          this.name = res.data.name;
          this.surname = res.data.surname;
          this.email = res.data.email;
          this.dateOfBirth = res.data.dateOfBirth;
          this.gender = res.data.gender;
          this.isPrivate = res.data.isPrivate;
          this.profilePicture = res.data.profilePicture
            ? res.data.profilePicture.path
            : "./static_resources/default_profile_pic.jpg";
          for (let i = 0; i < res.data.posts.length; i++) {
            if (res.data.posts[i].postType === "IMAGE") {
              this.images.push(res.data.posts[i].photo.path);
            }
          }
        }
      })
      .catch((err) => {
        console.error(err);
        window.location.replace("http://localhost:3000/#/error-403");
      });
  },
  methods: {
    saveChanges: function () {
      if (
        this.isValidUsername() &&
        this.isValidEmail() &&
        this.isValidName() &&
        this.isValidSurname()
      ) {
        axios
          .post("api/edit-profile", {
            username: this.username,
            name: this.name,
            surname: this.surname,
            email: this.email,
            dateOfBirth: this.dateOfBirth,
            gender: this.gender,
            isPrivate: this.isPrivate,
            profilePicture: this.profilePicture,
          })
          .then((res) => {
            alert(res.data);
            window.location.href = "#/regular/profile/" + this.username;
          })
          .catch((err) => {
            console.error(err);
            alert(err.data);
          });
      }
      this.editSent = true;
    },
    changePassword: function () {
      if (this.doPasswordsMatch())
        axios
          .post("api/change-password", {
            password: this.oldPassword,
            newPassword: this.newPassword,
            repeatPassword: this.repeatPassword,
          })
          .then((res) => {
            alert(res.data);
            this.oldPassword = "";
            this.newPassword = "";
            this.repeatPassword = "";
          })
          .catch((err) => {
            console.error(err);
            alert(res.data);
          });
      else this.editSent = true;
    },
    deleteProfile: function () {
      axios
        .delete("api/delete-profile", {})
        .then((res) => {
          window.location.replace("http://localhost:3000/#/");
        })
        .catch((err) => {
          console.error(err);
        });
    },
    isValidEmail: function () {
      return String(this.email)
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
    },
    doPasswordsMatch: function () {
      return this.newPassword === this.repeatPassword;
    },
    isValidUsername: function () {
      return String(this.username).match(/^[a-zA-Z0-9_-]{4,20}$/);
    },
    isValidName: function () {
      return String(this.name).match(/(^[a-zA-Z][a-zA-Z\s]{2,20}[a-zA-Z]$)/);
    },
    isValidSurname: function () {
      return String(this.surname).match(/(^[a-zA-Z][a-zA-Z\s]{2,20}[a-zA-Z]$)/);
    },
    selectImage: function (image) {
      this.profilePicture = image;
    },
  },
});
