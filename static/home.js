Vue.component("home-page", {
  data: function () {
    return {};
  },
  template: `
    <div>
      <home-navbar></home-navbar>
      <user-list></user-list>
    </div>
    `,
});
