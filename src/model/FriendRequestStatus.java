package model;

public enum FriendRequestStatus {
	WAITING,
	ACCEPTED,
	REJECTED
}
