package model;

import java.util.List;

public class Chat {

	public Chat() {
		// TODO Auto-generated constructor stub
	}
	
	private String user1;
	private String user2;
	
	private List<Message> messages;

	public String getUser1() {
		return user1;
	}

	public void setUser1(String user1) {
		this.user1 = user1;
	}

	public String getUser2() {
		return user2;
	}

	public void setUser2(String user2) {
		this.user2 = user2;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	

}
