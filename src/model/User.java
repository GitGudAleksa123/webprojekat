package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import dtos.SearchDTO;
import dtos.UserDTO;

public class User {
	private String username; // this is unique
	private String password;
	private String email;
	private String name;
	private String surname;
	private LocalDate dateOfBirth;
	private Photo profilePicture;
	private boolean isPrivate;
	private UserType type;
	private Gender gender;
	private List<Post> posts;
	private List<Message> messages;
	private List<FriendRequest> friendRequests;
	private List<String> friends;
	private boolean isDeleted;
	private boolean hasUnreadMessage;

	public User() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Photo getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(Photo profilePicture) {
		this.profilePicture = profilePicture;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public List<Post> getPosts() {
		if(posts != null) {
			posts.sort(new Comparator<Post>() {
				@Override
				public int compare(Post o1, Post o2) {
					if (o1.getTimeCreated().isAfter(o2.getTimeCreated())) return -1;
					else if(o1.getTimeCreated().isBefore(o2.getTimeCreated())) return 1;
					return 0;
				}
			});
			return posts;
		}
		return new ArrayList<Post>();
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public List<FriendRequest> getFriendRequests() {
		return friendRequests;
	}

	public void setFriendRequests(List<FriendRequest> friendRequests) {
		this.friendRequests = friendRequests;
	}

	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public static User createUserFromDTO(UserDTO userDTO) {
		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setName(userDTO.getName());
		user.setSurname(userDTO.getSurname());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setDateOfBirth(userDTO.getDateOfBirth());
		user.setGender(userDTO.getGender());
		user.setType(UserType.REGULAR);
		user.setDeleted(false);
		user.setPosts(new ArrayList<Post>());
		user.setFriendRequests(new ArrayList<FriendRequest>());
		user.setFriends(new ArrayList<String>());
		user.setProfilePicture(new Photo("./static_resources/default_profile_pic.jpg"));
		user.setHasUnreadMessage(false);
		return user;
	}

	public boolean isMatch(SearchDTO searchDTO) {
		String field = searchDTO.getField().toLowerCase();
		if (name.toLowerCase().contains(field) || surname.toLowerCase().contains(field)
				|| username.toLowerCase().contains(field))
			if (dateOfBirth.isAfter(searchDTO.getFrom()) && dateOfBirth.isBefore(searchDTO.getTo()))
				return true;
		return false;
	}

	public void addPost(Post post) {
		this.posts.add(post);
	}

	public boolean hasPosts() {
		if (this.posts == null)
			return false;
		return true;
	}

	public boolean hasUnreadMessage() {
		return hasUnreadMessage;
	}

	public void setHasUnreadMessage(boolean hasUnreadMessage) {
		this.hasUnreadMessage = hasUnreadMessage;
	}

}
