package model;

import java.time.LocalDateTime;

public class Message {
	private String text;
	private LocalDateTime dateOfMessage;
	private String sender;
	private String recipient;
	private boolean isDeleted;

	public Message() {
		super();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public LocalDateTime getDateOfMessage() {
		return dateOfMessage;
	}

	public void setDateOfMessage(LocalDateTime dateOfMessage) {
		this.dateOfMessage = dateOfMessage;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public boolean isRecipient(String candidate) {
		return this.recipient.equals(candidate);
	}
	
	public boolean isSender(String candidate) {
		return this.sender.equals(candidate);
	}

}
