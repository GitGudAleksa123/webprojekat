package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Post {
	private int id;
	private String description;
	private Photo photo;
	private List<Comment> comments;
	private LocalDateTime timeCreated;
	private boolean isDeleted;
	private String postType;
	private String author;

	public Post() {
		super();
	}

	public Post(int id, String description, Photo photo, List<Comment> comments, LocalDateTime timeCreated, String author,
			boolean isDeleted, String type) {
		super();
		this.id = id;
		this.description = description;
		this.photo = photo;
		this.comments = comments;
		this.timeCreated = timeCreated;
		this.setAuthor(author);
		this.isDeleted = isDeleted;
		this.postType = type;
	}
	public Post(int id, String description, Photo photo, LocalDateTime timeCreated, String author, String type) {
		super();
		this.id = id;
		this.description = description;
		this.photo = photo;
		this.timeCreated = timeCreated;
		this.setAuthor(author);
		this.comments = new ArrayList<Comment>();
		this.isDeleted = false;
		this.postType = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public LocalDateTime getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(LocalDateTime timeCreated) {
		this.timeCreated = timeCreated;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

}
