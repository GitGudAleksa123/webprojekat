package model;

public class Photo {
	private String path;
	private boolean isDeleted;

	public Photo() {
		super();
	}
	
	public Photo(String path) {
		this.path = path;
		this.isDeleted = false;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
