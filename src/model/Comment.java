package model;

import java.time.LocalDateTime;

public class Comment {
	private int id;
	private String text;
	private LocalDateTime dateOfComment; // ovo mozda promeniti na neku drugi date objekat po potrebi
	private boolean edited;
	private LocalDateTime dateOfEdit;
	private String author;
	private boolean isDeleted;

	public Comment() {
		super();
	}
	

	public Comment(int id, String text, LocalDateTime dateOfComment, String author) {
		super();
		this.id = id;
		this.text = text;
		this.dateOfComment = dateOfComment;
		this.author = author;
		this.dateOfEdit = null;
		this.edited = false;
		this.isDeleted = false;
	}


	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public LocalDateTime getDateOfComment() {
		return dateOfComment;
	}

	public void setDateOfComment(LocalDateTime dateOfComment) {
		this.dateOfComment = dateOfComment;
	}

	public boolean isEdited() {
		return edited;
	}

	public void setEdited(boolean edited) {
		this.edited = edited;
	}

	public LocalDateTime getDateOfEdit() {
		return dateOfEdit;
	}

	public void setDateOfEdit(LocalDateTime dateOfEdit) {
		this.dateOfEdit = dateOfEdit;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
