package utils;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * This is a custom deserializer which makes it possible to deserialize LocalDateTime
 * from JSON with a given custom DateTimeFormatter.
 * 
 * e.g. 
 * 
 * 	"date" : "01.01.2000. 10:10:10"
 * 
 * instead of 
 * 	
 * 	{"date":{"year":2000,"month":1,"day":1},"time":{"hour":10,"minute":10,"second":10,"nano":0}}
 * 
 * @author Ramesh Fadatare (www.javaguides.net)
 *
 */
public class LocalDateTimeDeserializer implements JsonDeserializer <LocalDateTime> {
    
	@Override
    public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
    throws JsonParseException {
        return LocalDateTime.parse(json.getAsString(),
            DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss"));
    }

}
