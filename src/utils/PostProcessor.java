package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.Part;

public class PostProcessor {

	
	public static void writeImage(String path, String filename, Part part) throws IOException {
	    OutputStream out = null;
	    InputStream filecontent = null;

	    try {
	    	System.out.println(path + filename);
	    	File file = new File(path + filename);
	    	file.getParentFile().mkdirs();
	    	file.createNewFile();
	        out = new FileOutputStream(file);
	        filecontent = part.getInputStream();

	        int read = 0;
	        final byte[] bytes = new byte[1024];

	        while ((read = filecontent.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	    } catch (FileNotFoundException fne) {
	    	fne.printStackTrace();
	    } finally {
	        if (out != null) {
	            out.close();
	        }
	        if (filecontent != null) {
	            filecontent.close();
	        }
	    }
	}
}
