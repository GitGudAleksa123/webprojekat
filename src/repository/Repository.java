package repository;

import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Post;
import model.User;

public class Repository {

	private final static String USERS = "data/users.json";
	
	public Repository() {
	}
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public static List<User> getAllUsers() {
		
		try {
		    Reader reader = Files.newBufferedReader(Paths.get(USERS));
		    List<User> users = new ArrayList<User>(Arrays.asList(gson.fromJson(reader, User[].class)));
		    reader.close();
		    return users;

		} catch (Exception ex) {
		    ex.printStackTrace();
		    System.out.println("SJEBALA MI SE BAZA");
		}
		return new ArrayList<User>();
	}
	
	public static void saveUser(User u) {
		List<User> users = getAllUsers();
		users.add(u);
		saveAllUsers(users);
	}
	
	public static void updateUsers(List<User> us) {
		List<User> users = getAllUsers();
		List<User> changedUsers = new ArrayList<User>();
		
		for (User user : users) {
			boolean userFound = false;
			for (User u : us) {
				if(user.getUsername().equals(u.getUsername())) {
					changedUsers.add(u);
					userFound = true;
					break;
				}
			}
			if(!userFound) changedUsers.add(user);
		}
		saveAllUsers(changedUsers);
	}
	public static void updateUser(User u) {
		List<User> users = getAllUsers();
		List<User> changedUsers = new ArrayList<User>();
		for (User user : users) {
			if(user.getUsername().equals(u.getUsername())) changedUsers.add(u);
			else changedUsers.add(user);
		}
		saveAllUsers(changedUsers);
	}
	
	public static void saveAllUsers(List<User> users) {
		try {
		    Writer writer = Files.newBufferedWriter(Paths.get(USERS));
		    gson.toJson(users, writer);
		    writer.close();
		} catch (Exception ex) {
		    ex.printStackTrace();
		}
	}
	public static void deleteUser(User u) {
		List<User> users = getAllUsers();
		List<User> changedUsers = new ArrayList<User>();
		for (User user : users) {
			if(user.getUsername().equals(u.getUsername())) user.setDeleted(true);
			changedUsers.add(user);
			
		}
		saveAllUsers(users);
	}
	public static List<Post> getPosts(String username) {
		List<Post> posts = new ArrayList<Post>();
		for (User user : getAllUsers()) {
			if(user.getUsername().equals(username)) posts.addAll(user.getPosts());
			break;
		}
		return posts;
	}
	
	public static User getUserByUsername(String username) {
		List<User> users = getAllUsers();
		for (User user : users) {
			if(user.getUsername().equals(username)) return user;
		}
		return null;
	}
}
