package services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import model.Comment;
import model.Message;
import model.Photo;
import model.Post;
import model.User;
import repository.Repository;

public class PostService {

	public PostService() {
		
	}
	public static List<Post> getFeed(User user) {
		List<Post> posts = new ArrayList<Post>();
		posts.addAll(UserService.getUser(user.getUsername()).getPosts());
		for (String friend : user.getFriends()) {
			posts.addAll(UserService.getUser(friend).getPosts());
		}
		posts.sort(new Comparator<Post>() {
			@Override
			public int compare(Post o1, Post o2) {
				if (o1.getTimeCreated().isAfter(o2.getTimeCreated())) return -1;
				else if(o1.getTimeCreated().isBefore(o2.getTimeCreated())) return 1;
				return 0;
			}
		});
		return posts;
	}
	public static Post createPost(User author, String description, LocalDateTime dateOfPost, String photoPath, String type) {
		Post post = new Post(UserService.generatePostId(), description, new Photo(photoPath), dateOfPost, author.getUsername(), type);
		if(!author.hasPosts()) author.setPosts(new ArrayList<Post>());
		author.getPosts().add(post);
		Repository.updateUser(author);
		return post;
	}
	public static void updatePost(String text, String owner, int postID) {
		User user = UserService.getUser(owner);
		for (Post post : user.getPosts()) {
			if (post.getId() == postID) {
				post.setDescription(text);
				break;
			}
		}
		Repository.updateUser(user);
	}
	public static void deletePost(String owner, int postID) {
		User user = UserService.getUser(owner);
		for (Post p : user.getPosts()) {
			if (p.getId() == postID) {
				p.setDeleted(true);
				break;
			}
		}
		Repository.updateUser(user);
	}

}
