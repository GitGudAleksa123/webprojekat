package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import dtos.ProfileChangeDTO;
import dtos.SearchDTO;

import model.Photo;
import model.Post;
import model.User;
import repository.Repository;

public class UserService {

	public UserService() {
	}
	
	public static void editProfile(User user, ProfileChangeDTO profileEditDTO) {
		user.setDateOfBirth(profileEditDTO.getDateOfBirth());
		user.setName(profileEditDTO.getName());
		user.setSurname(profileEditDTO.getSurname());
		user.setEmail(profileEditDTO.getEmail());
		user.setGender(profileEditDTO.getGender());
		user.setPrivate(profileEditDTO.isPrivate());
		user.setProfilePicture(new Photo(profileEditDTO.getProfilePicture()));
		Repository.updateUser(user);
		System.out.println("Successfully edited profile.");
	}
	
	public static void changePassword(User user, ProfileChangeDTO profileChangeDTO) {
		user.setPassword(profileChangeDTO.getNewPassword());
		System.out.println(user.getPassword());
		Repository.updateUser(user);
		System.out.println("Successfully changed password.");
	}
	public static void deleteProfile(User user) {
		Repository.deleteUser(user);
		System.out.println("Successfully deleted profile.");
	}
	
	public static List<User> getFriends(User user) {
		List<User> friends = new ArrayList<User>();
		for (String username : user.getFriends()) {
			for (User u: Repository.getAllUsers()) {
				if(u.getUsername().equals(username)) friends.add(u);
			}
		}
		return friends;
	}
	public static List<User> getPublicUsers(User u) {
		List<User> publicUsers = new ArrayList<User>();
		if(u == null) {
			for (User user : Repository.getAllUsers()) {
				if(user.isPrivate()) continue;
				publicUsers.add(user);
			}
		} else {
			for (User user : Repository.getAllUsers()) {
				if(user.isPrivate() || user.getUsername().equals(u.getUsername())) continue;
				publicUsers.add(user);
			}
		}
		return publicUsers;
	}
	
	public static User getUser(String username) {
		List<User> users = Repository.getAllUsers();
		for (User u : users) {
			if (u.getUsername().equals(username)) {
				return u;
			}
		}
		return null;
	}

	public static List<User> search(SearchDTO searchDTO) {
		List<User> result = new ArrayList<User>();
		List<User> allUsers = Repository.getAllUsers();
		for (User user : allUsers) {
			if(user.isPrivate()) continue;
			if(user.isMatch(searchDTO)) {
				result.add(user);
			}
		}
		return result;
	}
	public static List<User> searchByDate() {
		List<User> result = new ArrayList<User>();
		return result;
	}

	public static void readMessage(User user) {
		user.setHasUnreadMessage(false);
		Repository.updateUser(user);
	}

	public static int generatePostId() {
		int retVal = 0;
		for (User user : Repository.getAllUsers()) {
			try {
			retVal += user.getPosts().size();
			} catch (Exception e) {
				retVal += retVal;
			}
		}
		return retVal;
	}

	public static HashMap<String, String> getProfilePhotos() {
		HashMap<String, String> profilePhotos = new HashMap<String, String>();
		List<User> allUsers = Repository.getAllUsers();
		for (User user : allUsers) {
			System.out.println(user.getProfilePicture());
			try {
				profilePhotos.put(user.getUsername(), user.getProfilePicture().getPath());
			} catch (Exception e) {
				profilePhotos.put(user.getUsername(), "./static_resources/default_profile_pic.jpg");
			}
		}
		return profilePhotos;
	}

	public static int generateCommentId() {
		int retVal = 0;
		for (User user : Repository.getAllUsers()) {
			try {
				for (Post p : user.getPosts()) {
					retVal += p.getComments().size();
				}
			} catch (Exception e) {
				retVal += 0;
			}
		}
		return retVal;
	}

	public static void block(String username) {
		User user = getUser(username);
		user.setDeleted(true);
		Repository.updateUser(user);
	}

	public static void unblock(String username) {
		User user = getUser(username);
		user.setDeleted(false);
		Repository.updateUser(user);
	}
}
