package services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import dtos.FriendDTO;
import model.FriendRequest;
import model.FriendRequestStatus;
import model.User;
import repository.Repository;

public class FriendService {

	public FriendService() {
		
	}
	public static FriendRequest createRequest(FriendDTO dto) {
		User recipient = UserService.getUser(dto.getRecipient());
		FriendRequest fr = new FriendRequest();
		fr.setRecipientUsername(dto.getRecipient());
		fr.setSenderUsername(dto.getSender());
		fr.setStatus(FriendRequestStatus.WAITING);
		fr.setDeleted(false);
		fr.setDateSent(LocalDateTime.now());
		if (recipient.getFriendRequests() == null) {
			recipient.setFriendRequests(new ArrayList<FriendRequest>());
		}
		recipient.getFriendRequests().add(fr);
		Repository.updateUser(recipient);
		return fr;
	}
	public static void updateRequest (FriendDTO dto, FriendRequestStatus status) {
		User recipient = UserService.getUser(dto.getRecipient());
		User sender = UserService.getUser(dto.getSender());
		for(FriendRequest fr : recipient.getFriendRequests()) {
			if (fr.getSenderUsername().equals(sender.getUsername())) {
				fr.setStatus(status);
				fr.setDeleted(true);
				if (status == FriendRequestStatus.ACCEPTED) {
					if (recipient.getFriends() == null) {
						recipient.setFriends(new ArrayList<String>());
					}
					if (sender.getFriends() == null) {
						sender.setFriends(new ArrayList<String>());
					}
					recipient.getFriends().add(sender.getUsername());
					sender.getFriends().add(recipient.getUsername());
				}
			}
		}
		List<User> save = new ArrayList<User>();
		save.add(sender);
		save.add(recipient);
		Repository.updateUsers(save);
	}
	
	public static void removeFriend(FriendDTO dto) {
		User recipient = UserService.getUser(dto.getRecipient());
		User sender = UserService.getUser(dto.getSender());
		recipient.getFriends().remove(sender.getUsername());
		sender.getFriends().remove(recipient.getUsername());
		List<User> save = new ArrayList<User>();
		save.add(sender);
		save.add(recipient);
		Repository.updateUsers(save);
	}

}
