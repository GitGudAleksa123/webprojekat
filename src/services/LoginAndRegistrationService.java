package services;

import dtos.UserDTO;
import model.User;
import repository.Repository;

public class LoginAndRegistrationService {
	public LoginAndRegistrationService() {
		
	}
	public static User login(UserDTO userDTO) {
		for (User user : Repository.getAllUsers()) {
			if (user.getUsername().equals(userDTO.getUsername()) && user.getPassword().equals(userDTO.getPassword())) {
				if (user.isDeleted()) {
					System.out.println("User is blocked.");
				} else {
					System.out.println("User successfully logged in.");
				}
				return user;
			}
		}
		System.out.println("Couldn't log in.");
		return null;
	}
	public static User register(UserDTO userDTO) {
		if(!userDTO.getPassword().equals(userDTO.getRepeatPassword())) return null;
		for (User user : Repository.getAllUsers()) {
			if(user.getUsername().equals(userDTO.getUsername())) {
				System.out.println("Couldn't register user.");
				return null;
			}
		}
		User user = User.createUserFromDTO(userDTO);
		Repository.saveUser(user);
		System.out.println("User successfully registered.");
		return user;
	}

}
