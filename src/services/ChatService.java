package services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import model.Message;
import model.User;
import repository.Repository;

public class ChatService {

	public ChatService() {
	}
	
	public static void addMessage(Message message) {
		User sender = null;
		User recipient = null;
		List<User> allUsers = Repository.getAllUsers();
		for (User user : allUsers) {
			if(user.getUsername().equals(message.getSender())) sender = user;
			if(user.getUsername().equals(message.getRecipient())) recipient = user;
		}
		if(sender.getMessages() == null) sender.setMessages(new ArrayList<Message>());
		if(recipient.getMessages() == null) recipient.setMessages(new ArrayList<Message>());

		sender.getMessages().add(message);
		recipient.getMessages().add(message);
		recipient.setHasUnreadMessage(true);
		List<User> save = new ArrayList<User>();
		save.add(sender);
		save.add(recipient);
		Repository.updateUsers(save);
		System.out.println("Message saved!");
	}
	
	public static List<Message> getMessages(String sender, String recipient) {
		List<Message> messages = new ArrayList<Message>();
		User senderObject = Repository.getUserByUsername(sender);
		if(senderObject == null) return messages;
		if(senderObject.getMessages() == null) return messages;
		for (Message message : senderObject.getMessages()) {
			if(message.isRecipient(recipient) || message.isSender(recipient)) messages.add(message);
		}
		if(messages.isEmpty()) return messages;
		messages.sort(new Comparator<Message>() {
			@Override
			public int compare(Message o1, Message o2) {
				if (o1.getDateOfMessage().isAfter(o2.getDateOfMessage())) return -1;
				else if(o1.getDateOfMessage().isBefore(o2.getDateOfMessage())) return 1;
				return 0;
			}
		});
		return messages;
	}

}
