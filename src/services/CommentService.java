package services;

import java.time.LocalDateTime;

import model.Comment;
import model.Post;
import model.User;
import repository.Repository;

public class CommentService {

	public CommentService() {
		
	}
	public static Comment createComment(String text, String owner, User author, int postID) {
		int id = UserService.generateCommentId();
		User user = UserService.getUser(owner);
		Comment c = new Comment(id, text, LocalDateTime.now(), author.getUsername());
		for (Post p : user.getPosts()) {
			if (p.getId() == postID) {
				p.getComments().add(c);
			}
		}
		Repository.updateUser(user);
		return c;
	}

	public static void updateComment(String text, String owner, int postID, int commentID) {
		User user = UserService.getUser(owner);
		for (Post p : user.getPosts()) {
			if (p.getId() == postID) {
				for (Comment com : p.getComments()) {
					if (com.getId() == commentID) {
						com.setText(text);
						com.setDateOfEdit(LocalDateTime.now());
						com.setEdited(true);
						break;
					}
				}
			}
		}
		Repository.updateUser(user);
	}
	
	public static void deleteComment(String owner, int postID, int commentID) {
		User user = UserService.getUser(owner);
		for (Post p : user.getPosts()) {
			if (p.getId() == postID) {
				for (Comment com : p.getComments()) {
					if (com.getId() == commentID) {
						com.setDeleted(true);
						break;
					}
				}
			}
		}
		Repository.updateUser(user);
	}
}
