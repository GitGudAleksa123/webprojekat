package controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Message;
import services.ChatService;
import utils.LocalDateDeserializer;
import utils.LocalDateSerializer;
import utils.LocalDateTimeDeserializer;
import utils.LocalDateTimeSerializer;

@WebSocket
public class Chat {
 
    private static Gson gson = new GsonBuilder()
			.registerTypeAdapter(LocalDate.class, new LocalDateSerializer())
			.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer())
			.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
			.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer()).create();

    private static Map<Session, String> chatMap = new ConcurrentHashMap<>();
    

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        String username = Controller.currentUser;
        System.out.println(username + " Otvorio konekciju!");
        chatMap.put(user, username);
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        chatMap.remove(user);
    }

    @OnWebSocketMessage
    public void onMessage(Session sender, String json) {
    	Message message = gson.fromJson(json, Message.class);
    	System.out.println(gson.toJson(message));
    	message.setDateOfMessage(LocalDateTime.now());
        sendMessage(message);
    }
    
    public static void sendMessage(Message message) {
    	ChatService.addMessage(message);
    	for (Map.Entry<Session, String> entry : chatMap.entrySet()) {
			if(entry.getKey().isOpen() && entry.getValue().equals(message.getRecipient())) {
				try {
					entry.getKey().getRemote().sendString(gson.toJson(message));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    }

}
