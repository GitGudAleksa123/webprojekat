package controller;

import static spark.Spark.*;

import java.io.File;
import java.io.IOException;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import model.FriendRequestStatus;
import model.User;
import model.UserType;
import utils.PostProcessor;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dtos.CommentDTO;
import dtos.FriendDTO;
import dtos.ProfileChangeDTO;
import dtos.SearchDTO;
import dtos.UserDTO;
import services.CommentService;
import services.FriendService;
import services.ChatService;
import services.LoginAndRegistrationService;
import services.PostService;
import services.UserService;
import spark.Session;
import utils.LocalDateDeserializer;
import utils.LocalDateSerializer;
import utils.LocalDateTimeDeserializer;
import utils.LocalDateTimeSerializer;

public class Controller {

	public Controller() {
	}

	private static Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateSerializer())
			.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer())
			.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
			.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer()).create();

	public static String currentUser;

	public static void main(String[] args) {
		port(3000);
		try {
			staticFiles.externalLocation(new File("./static").getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		staticFiles.expireTime(600L);
		webSocket("/chat", Chat.class);
		init();

		post("api/login", (req, res) -> {
			String body = req.body();
			UserDTO userDTO = gson.fromJson(body, UserDTO.class);
			User user = LoginAndRegistrationService.login(userDTO);
			if (user == null) {
				res.status(400);
				return "Invalid username or password.";
			} else if (user.isDeleted()) {
				res.status(400);
				return "User blocked.";
			}
			spark.Session session = req.session();
			session.attribute("user", user);
			currentUser = user.getUsername();
			return "Successfull login.";
		});

		post("api/register", (req, res) -> {
			String body = req.body();
			UserDTO userDTO = gson.fromJson(body, UserDTO.class);
			User user = LoginAndRegistrationService.register(userDTO);
			if (user == null) {
				res.status(400);
				return "Couldn't register user.";
			}
			Session session = req.session();
			session.attribute("user", user);
			return "Successfull registration.";
		});
		post("api/createPost", (req, res) -> {
			res.type("application/json");
			spark.Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			MultipartConfigElement multipartConfigElement = new MultipartConfigElement("./data");
			req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
			String fullPath = null;
			Part file = req.raw().getPart("file");
			if (file != null) {
				String extension = "."
						+ file.getSubmittedFileName().split("\\.")[file.getSubmittedFileName().split("\\.").length - 1];
				String generatedFileName = user.getUsername() + String.valueOf(user.getPosts().size() + 1) + extension;
				PostProcessor.writeImage("./static/users/" + user.getUsername() + "/", generatedFileName, file);
				fullPath = "/users/" + user.getUsername() + "/" + generatedFileName;
			}
			String text = req.queryParams("text");
			String type = req.queryParams("type");
			return gson.toJson(PostService.createPost(user, text, LocalDateTime.now(), fullPath, type));
		});
		post("api/updatePost", (req, res) -> {
			res.type("application/json");
			CommentDTO postDTO = gson.fromJson(req.body(), CommentDTO.class);
			spark.Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			PostService.updatePost(postDTO.getText(), postDTO.getOwner(), postDTO.getPostID());
			res.status(200);
			return "Successfull update";
		});

		post("api/deletePost", (req, res) -> {
			spark.Session ss = req.session(true);
			CommentDTO postDTO = gson.fromJson(req.body(), CommentDTO.class);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			PostService.deletePost(postDTO.getOwner(), postDTO.getPostID());
			res.status(200);
			return "Successfull delete";
		});
		post("api/postComment", (req, res) -> {
			res.type("application/json");
			CommentDTO commentDTO = gson.fromJson(req.body(), CommentDTO.class);
			spark.Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			res.status(200);
			return gson.toJson(CommentService.createComment(commentDTO.getText(), commentDTO.getOwner(), user,
					commentDTO.getPostID()));
		});
		post("api/updateComment", (req, res) -> {
			res.type("application/json");
			CommentDTO commentDTO = gson.fromJson(req.body(), CommentDTO.class);
			spark.Session ss = req.session(true);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			CommentService.updateComment(commentDTO.getText(), commentDTO.getOwner(), commentDTO.getPostID(),
					commentDTO.getCommentID());
			res.status(200);
			return gson.toJson(LocalDateTime.now());
		});

		post("api/deleteComment", (req, res) -> {
			spark.Session ss = req.session(true);
			CommentDTO commentDTO = gson.fromJson(req.body(), CommentDTO.class);
			User user = ss.attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			CommentService.deleteComment(commentDTO.getOwner(), commentDTO.getPostID(), commentDTO.getCommentID());
			res.status(200);
			return "Successfull deleted";
		});

		get("api/current-user", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			return gson.toJson(UserService.getUser(user.getUsername()));
		});
		post("api/get-user", (req, res) -> {
			String body = req.body();
			UserDTO userDTO = gson.fromJson(body, UserDTO.class);
			User user = UserService.getUser(userDTO.getUsername());
			if (user == null) {
				res.status(403);
				return "User doesn't exist!";
			}
			return gson.toJson(user);
		});
		post("api/logout", (req, res) -> {
			req.session().invalidate();
			return "Successfull logout.";
		});

		post("api/edit-profile", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			String body = req.body();
			ProfileChangeDTO profileEditDTO = gson.fromJson(body, ProfileChangeDTO.class);
			UserService.editProfile(user, profileEditDTO);
			return "Successfully edited profile.";
		});

		post("api/change-password", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			String body = req.body();
			ProfileChangeDTO profileEditDTO = gson.fromJson(body, ProfileChangeDTO.class);
			if (!profileEditDTO.getPassword().equals(user.getPassword())) {
				res.status(400);
				return "Old password isn't correct!";
			}
			if (!profileEditDTO.getNewPassword().equals(profileEditDTO.getRepeatPassword())) {
				res.status(400);
				return "Passwords don't match!";
			}
			UserService.changePassword(user, profileEditDTO);
			return "Successfully edited profile.";
		});

		delete("api/delete-profile", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			return "";

		});

		get("api/get-posts", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(403);
				return "You aren't logged in!";
			}
			return user.getPosts();
		});

		get("api/get-public-users", (req, res) -> {
			User user = req.session().attribute("user");
			return gson.toJson(UserService.getPublicUsers(user));
		});

		get("api/get-friend-posts", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}

			return gson.toJson(PostService.getFeed(user));

		});
		get("api/get-my-posts", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}
			if (!user.hasPosts()) {
				return "[]";
			}
			return gson.toJson(user.getPosts());
		});

		get("api/get-profile-photos", (req, res) -> {
			res.status(200);
			return gson.toJson(UserService.getProfilePhotos());
		});

		post("api/sendFriendRequest", (req, res) -> {
			res.type("application/json");
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}
			FriendDTO friendDTO = gson.fromJson(req.body(), FriendDTO.class);
			res.status(200);
			return gson.toJson(FriendService.createRequest(friendDTO));
		});
		post("api/acceptFriendRequest", (req, res) -> {
			res.type("application/json");
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}
			FriendDTO friendDTO = gson.fromJson(req.body(), FriendDTO.class);
			FriendService.updateRequest(friendDTO, FriendRequestStatus.ACCEPTED);
			res.status(200);
			return "Friend Request Accepted";
		});
		post("api/rejectFriendRequest", (req, res) -> {
			res.type("application/json");
			FriendDTO friendDTO = gson.fromJson(req.body(), FriendDTO.class);
			FriendService.updateRequest(friendDTO, FriendRequestStatus.REJECTED);
			return "Friend Request Rejected";
		});

		post("api/messages", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}
			currentUser = user.getUsername();
			String recipient = req.body();
			// ovde postoji mogucnost
			return gson.toJson(ChatService.getMessages(user.getUsername(), recipient));
		});

		post("api/search", (req, res) -> {
			SearchDTO searchDTO = gson.fromJson(req.body(), SearchDTO.class);
			return gson.toJson(UserService.search(searchDTO));
		});

		get("api/read-message", (req, res) -> {
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}
			User u = UserService.getUser(user.getUsername());
			UserService.readMessage(u);
			return "Succesfully read message!";
		});

		post("api/removeFriend", (req, res) -> {
			res.type("application/json");
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			}
			FriendDTO friendDTO = gson.fromJson(req.body(), FriendDTO.class);
			FriendService.createRequest(friendDTO);
			return "Friend Removed";
		});

		post("api/block", (req, res) -> {
			res.type("application/json");
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			} else if (user.getType() != UserType.ADMIN) {
				res.status(400);
				return "You aren't ADMIN!";
			}
			UserDTO userDTO = gson.fromJson(req.body(), UserDTO.class);
			UserService.block(userDTO.getUsername());
			return "User Blocked";
		});

		post("api/unblock", (req, res) -> {
			res.type("application/json");
			User user = req.session().attribute("user");
			if (user == null) {
				res.status(400);
				return "You aren't logged in!";
			} else if (user.getType() != UserType.ADMIN) {
				res.status(400);
				return "You aren't ADMIN!";
			}
			UserDTO userDTO = gson.fromJson(req.body(), UserDTO.class);
			UserService.unblock(userDTO.getUsername());
			return "User Unblocked";
		});
	}
}
